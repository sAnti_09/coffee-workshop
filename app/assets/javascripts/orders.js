$(function () {
  var products = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '/products?q=%QUERY',
      wildcard: '%QUERY'
    }
  })

  $('.order .search-products').typeahead(null, {
    name: 'products',
    display: 'name',
    source: products
  }).on('typeahead:select', function (ev, product) {
    $.get($(this).data('url') + '?product_id=' + product.id.$oid, function (data) {
      $('.order .items').append(data)
    })
  })

  $('body').on('change keyup', ".item input[type='number']", function () {
    var container = $(this).closest('.item')
    container.find('.total').text($(this).val() * container.find('.price').val())
  })
})