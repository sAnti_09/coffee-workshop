class Product
  include Mongoid::Document
  include Mongoid::Timestamps::Short

  field :name, type: String
  field :description, type: String
  field :price, type: Float
  field :stock, type: Integer
  field :photo, type: String

  attr_accessor :picture

  validates :name, presence: true, uniqueness: true
  validates :price, numericality: {greater_than: 0}
  validates :stock, numericality: {only_integer: true}

  has_many :order_items

  before_save :save_image

  def save_image
    return unless picture
    dir = Rails.root.join('public', 'uploads').to_s
    Dir.mkdir(dir) unless Dir.exist?(dir)

    uploaded_io = picture
    file = dir << '/' << uploaded_io.original_filename
    File.open(file, 'wb') do |file|
      file.write(uploaded_io.read)
    end

    self.photo = '/uploads/' << uploaded_io.original_filename
  end
end
