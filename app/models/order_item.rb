class OrderItem
  include Mongoid::Document
  include Mongoid::Timestamps::Short

  field :unit_price, type: Float
  field :quantity, type: Integer, default: 1
  field :total, type: Float

  validates :unit_price, numericality: {greater_than: 0}
  validates :quantity, numericality: {only_integer: true}
  validates :total, numericality: {greater_than: 0}

  belongs_to :order
  belongs_to :product

  def initialize(attrs = nil)
    super
    self.unit_price = product.price
    self.total = unit_price * quantity
  end
end