class Order
  include Mongoid::Document
  include Mongoid::Timestamps::Short

  validates :customer, presence: true
  # validates :order_items, presence: true

  belongs_to :customer
  has_many :order_items

  accepts_nested_attributes_for :order_items

  def remove_flagged_items(params)
    if params[:order_items_attributes].blank?
      order_items.destroy_all
    else
      item_ids = params[:order_items_attributes].map { |id, _| id }
      order_items.each do |item|
        item.destroy unless item_ids.include? item.id.to_s
      end
    end
  end

  def total
    order_items.inject (0) { |sum, i| sum + i.total }
  end
end
